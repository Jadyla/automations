import os
import re
from datetime import date
from datetime import datetime


year = str(date.today())[0:4]


def get_month_number(month_number):
    if month_number == 'Jan':
        return '01'
    elif month_number == 'Fab':
        return '02'
    elif month_number == 'Mar':
        return '03'
    elif month_number == 'Apr':
        return '04'
    elif month_number == 'May':
        return '05'
    elif month_number == 'Jun':
        return '06'
    elif month_number == 'Jul':
        return '07'
    elif month_number == 'Aug':
        return '08'
    elif month_number == 'Sep':
        return '09'
    elif month_number == 'Oct':
        return '10'
    elif month_number == 'Nov':
        return '11'
    elif month_number ==  'Dec':
        return '12'
    

def get_journal():
    if not os.path.exists("/controllar/aux.txt"):
        os.system("touch /controllar/aux.txt")
    os.system("echo \"$(journalctl -u broadlink-mqtt --since \"36 hours ago\")\" > /controllar/aux.txt")

    with open("/controllar/aux.txt") as file:
        journal = file.read().splitlines()
    os.system("rm /controllar/aux.txt")
    return journal


def get_broadlink_error(errors):
    count = 0
    for error in errors:
        errors_split = error.split(" ")
        month = errors_split[0]
        day = errors_split[1]
        time = errors_split[2]
        try:
            broadlink_mac = error.split("/", 2)[1]
            command = error.split("/", 2)[2].split(" ", 1)[0]
        except:
            broadlink_mac = "none"
            command = "none"
        error = (month + " " + day + " " + time + " " + broadlink_mac + " " + command)
        errors[count] = error
        count+=1
    return errors


def look_at_journal(line, mac):
    for i in range(len(journal)):
        if mac in journal[line+i] and "Connected" in journal[line+i]:
            return line+i


def set_output(journal, error):
    count = 0
    errors_count = 0
    error_split = error.split(" ")
    error_month = error_split[0]
    error_day = error_split[1]
    error_time = error_split[2]
    error_broadlink_mac = error_split[3]
    for line in journal:
        if error_month in line and error_day in line and error_time in line:
            if error_split[3] != "none":
                if error_split[3] in line and error_split[4] in line:
                    line_return = look_at_journal(count, error_broadlink_mac)
                    return_month = journal[line_return].split(" ")[0]
                    return_day = journal[line_return].split(" ")[1]
                    return_time = journal[line_return].split(" ")[2]
                    error_moment = ('{}/{}/{} {}'.format(year, get_month_number(error_month), error_day, error_time))
                    return_moment = ('{}/{}/{} {}'.format(year, get_month_number(return_month), return_day, return_time))
                    f = '%Y/%m/%d %H:%M:%S'
                    diff_time = (datetime.strptime(return_moment, f) - datetime.strptime(error_moment, f)).total_seconds()
                    print("error: " + str(error_time))
                    print("return: " + str(return_time))
                    print("diff: " + str(diff_time) + "\n")
                    if diff_time > 10:
                        errors_count+=1
                        with open("/controllar/broadlink-debug.log", 'a+') as f:
                            f.write(error_broadlink_mac + "  error_time: " + str(error_moment) + " | return_time: " + str(return_moment) + "\n")
        count+=1
    return errors_count


if __name__ == '__main__':

    journal = get_journal()
    # if be necesary to add new errors, add a '|' and error. Example:
    # "has no|ERROR|bla" (here we have three errors)
    # After this, if necessary, add the error informations on get_broadlink_error()
    errors_list = "has no"
    errors = []

    for error in journal:
        if(re.findall(errors_list, error)):
            errors.append(error)

    # the errror will be a string with: month, day
    # time, broadlink mac and command that failed
    errors = get_broadlink_error(errors)
    present_error = False
    for error in errors:
        errors_count = set_output(journal, error)
        if errors_count:
            present_error = True

    if not present_error:
        with open("/controllar/broadlink-debug.log", 'a+') as f:
            f.write(str(date.today()) + " without disconnections\n")
